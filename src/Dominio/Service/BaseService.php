<?php
/**
 * Created by PhpStorm.
 * User: diomedes
 * Date: 12/09/16
 * Time: 5:09 PM
 */

namespace Neoauto\Dominio\Service;

use Neoauto\Config\DependenceInjection;

class BaseService extends DependenceInjection
{
    /**
     * @var \Neoauto\Infraestructura\Dao\Factory\DaoFactoryZend
     */
    protected $daoFactoryZend;

    /**
     * @var \Neoauto\Infraestructura\Dao\Factory\DaoFactoryEloquent
     */
    protected $daoFactoryEloquent;

    public function __construct()
    {
        parent::__construct();
    }
    
    public function getDaoFactoryZend()
    {
        try {
            $this->daoFactoryZend = $this->getDependency('DaoFactoryZend');
            return $this->daoFactoryZend;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getDaoFactoryEloquent()
    {
        try {
            $this->daoFactoryEloquent = $this->getDependency('DaoFactoryEloquent');
            return $this->daoFactoryEloquent;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}