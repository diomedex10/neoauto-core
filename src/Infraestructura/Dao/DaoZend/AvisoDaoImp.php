<?php
namespace Neoauto\Infraestructura\Dao\DaoZend;

use Neoauto\Infraestructura\Dao\AvisoDao;
use Neoauto\Common\Adapter\Persistence\Zend\ZendRepository;

class AvisoDaoImp implements AvisoDao
{
    protected $repository;

    public function __construct(ZendRepository $repository)
    {
        $this->repository = $repository;
    }

    public function insert($data)
    {
        return " -- Insert desde Zend --";
    }

    public function update($id, $data)
    {

    }

    public function delete($id)
    {

    }

    public function findById($id)
    {

    }
    
}