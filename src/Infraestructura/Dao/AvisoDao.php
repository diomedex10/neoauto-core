<?php
/**
 * Created by PhpStorm.
 * User: diomedes
 * Date: 4/09/16
 * Time: 9:29 PM
 */

namespace Neoauto\Infraestructura\Dao;


interface AvisoDao
{
    public function insert($data);
    public function update($id, $data);
    public function delete($id);
    public function findById($id);
}