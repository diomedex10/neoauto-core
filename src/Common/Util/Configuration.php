<?php
namespace Neoauto\Common\Util;
require_once __DIR__.'/../../Lib/Spyc.php';

class Configuration
{
    public function __construct()
    {
    }

    public static function getAllConfig()
    {
        try {
            $reader    = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $readerIni = new \Zend\Config\Reader\Ini();

            $base         = $reader->fromFile(Server::getConfigEnviromentDir().'/base.config.yaml');
            $cache        = $reader->fromFile(Server::getConfigEnviromentDir().'/cache.config.yaml');
            $images       = $reader->fromFile(Server::getConfigEnviromentDir().'/images.config.yaml');
            $mail         = $reader->fromFile(Server::getConfigEnviromentDir().'/mail.config.yaml');
            $pagoefectivo = $reader->fromFile(Server::getConfigEnviromentDir().'/pagoEfectivo.config.yaml');
            $persistence  = $reader->fromFile(Server::getConfigEnviromentDir().'/persistence.config.yaml');
            $service      = $reader->fromFile(Server::getConfigEnviromentDir().'/service.config.yaml');
            $baseDynamic  = $readerIni->fromFile(Server::getConfigEnviromentDir().'/baseDynamic.config.ini');
            $pagoEfecDyn  = $readerIni->fromFile(Server::getConfigEnviromentDir().'/pagoEfectivoDynamic.config.ini');

            return array_merge_recursive($base, $cache, $images, $mail, $pagoefectivo, $persistence, $service, $baseDynamic, $pagoEfecDyn);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getConfigPersistence()
    {
        try {
            $reader = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $result = $reader->fromFile(Server::getConfigEnviromentDir().'/persistence.config.yaml');

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getConfigService()
    {
        try {
            $reader = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $result = $reader->fromFile(Server::getConfigEnviromentDir().'/service.config.yaml');

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getConfigBase()
    {
        try {
            $reader    = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $readerIni = new \Zend\Config\Reader\Ini();

            $result      = $reader->fromFile(Server::getConfigEnviromentDir().'/base.config.yaml');
            $baseDynamic = $readerIni->fromFile(Server::getConfigEnviromentDir().'/baseDynamic.config.ini');

            return array_merge_recursive($result, $baseDynamic);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getConfigMail()
    {
        try {
            $reader = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $result = $reader->fromFile(Server::getConfigEnviromentDir().'/mail.config.yaml');

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getConfigImages()
    {
        try {
            $reader = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $result = $reader->fromFile(Server::getConfigEnviromentDir().'/images.config.yaml');

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getConfigPagoEfectivo()
    {
        try {
            $reader    = new \Zend\Config\Reader\Yaml(array('Spyc','YAMLLoadString'));
            $readerIni = new \Zend\Config\Reader\Ini();

            $result  = $reader->fromFile(Server::getConfigEnviromentDir().'/pagoEfectivo.config.yaml');
            $dynamic = $readerIni->fromFile(Server::getConfigEnviromentDir().'/pagoEfectivoDynamic.config.ini');

            return array_merge_recursive($result, $dynamic);

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
