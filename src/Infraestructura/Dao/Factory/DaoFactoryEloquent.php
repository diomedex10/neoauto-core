<?php
namespace Neoauto\Infraestructura\Dao\Factory;

use Neoauto\Infraestructura\Dao\DaoEloquent\AvisoDaoImp;
 
class DaoFactoryEloquent
{
    public function __construct()
    {

    }
    
    public function getAviso() 
    {
        return new AvisoDaoImp();
    }
}