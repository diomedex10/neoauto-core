<?php
namespace Neoauto\Config;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class DependenceInjection
{
    protected $container;

    public function __construct()
    {
        try {
            $this->container = new ContainerBuilder();

            $adapterZend = new Definition('Neoauto\Common\Adapter\Persistence\Zend\ZendRepository');
            $adapterZend->setAutowired(true);
            $this->container->setDefinition('ZendRepository', $adapterZend);

            $avisoDaoZend = new Definition('Neoauto\Infraestructura\Dao\Factory\DaoFactoryZend', array(
                new Reference('ZendRepository'),
            ));
            $this->container->setDefinition('DaoFactoryZend', $avisoDaoZend);


            $avisoDaoEloquent = new Definition('Neoauto\Infraestructura\Dao\Factory\DaoFactoryEloquent', array(
                new Reference('ZendRepository'),
            ));
            $this->container->setDefinition('DaoFactoryEloquent', $avisoDaoEloquent);

        } catch (\Exception $e) {
            var_dump($e->getMessage());exit;
        }
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function getDependency($service)
    {
        return $this->container->get($service);
    }
}