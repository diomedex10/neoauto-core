<?php
 
namespace Neoauto\Common\Util;

class Server
{

    public static function getConfigDir()
    {
        $ruta = __DIR__ . "/../../Config";
        return realpath($ruta);
    }

    public static function getConfigEnviromentDir()
    {
        try {
            $configDir  = self::getConfigDir();
            $enviroment = self::getEnviroment();
            $result     = null;

            $envDir = array(
                'production'  => 'prod',
                'development' => 'dev',
                'local'       => 'local'
            );

            switch ($enviroment) {
                case 'production' :
                    $result = $configDir. '/'.$envDir[$enviroment];
                    break;
                case 'development' :
                    $result = $configDir. '/'.$envDir[$enviroment];
                    break;
                case 'local' :
                    $result = $configDir. '/'.$envDir[$enviroment];
                    break;
            }

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function getEnviroment ()
    {
        $enviroment = getenv('APPLICATION_ENV');

        if (empty($enviroment)) {
            throw new \Exception('Configuration enviroment null');
        }
        
        return $enviroment;
    }
}