<?php
namespace Neoauto\Infraestructura\Dao\Factory;

use Neoauto\Common\Adapter\Persistence\Zend\ZendRepository;

class DaoFactoryZend
{
    protected $repository;

    public function __construct(ZendRepository $repositiry)
    {
        $this->repository = $repositiry;
    }
    
    public function getAviso() 
    {
        return new \Neoauto\Infraestructura\Dao\DaoZend\AvisoDaoImp($this->repository);
    }
}